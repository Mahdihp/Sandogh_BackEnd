package main

import (
	"Sandogh_BackEnd/account"
	"Sandogh_BackEnd/common"
	"Sandogh_BackEnd/models"
	"Sandogh_BackEnd/models/dto"
	"Sandogh_BackEnd/monthly"
	"Sandogh_BackEnd/role"
	"fmt"
	"github.com/asaskevich/govalidator"
	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/kataras/iris"
	"github.com/kataras/iris/cache"
	"github.com/kataras/iris/context"
	"github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"os"
	"time"
)

var log = logrus.New()

func init() {
	viper.SetConfigFile(`config.json`)
	err := viper.ReadInConfig()

	if err != nil {
		panic(err)
	}

	if viper.GetBool(`debug`) {
		fmt.Println("Service RUN on DEBUG mode")
	}
	govalidator.SetFieldsRequiredByDefault(true)

	log.Formatter = new(logrus.JSONFormatter)
	log.Formatter = new(logrus.TextFormatter)                      //default
	log.Formatter.(*logrus.TextFormatter).DisableColors = false    // remove colors
	log.Formatter.(*logrus.TextFormatter).DisableTimestamp = false // remove timestamp from test output
	log.Level = logrus.TraceLevel
	log.Out = os.Stdout

}

func main() {

	db := InitDb()
	StartServer(db)
}

func InitDb() *gorm.DB {

	dbHost := viper.GetString(`database.host`)
	dbType := viper.GetString(`database.type`)
	dbPort := viper.GetString(`database.port`)
	dbUser := viper.GetString(`database.user`)
	dbPass := viper.GetString(`database.pass`)
	dbName := viper.GetString(`database.dbname`)
	//fmt.Println(dbUser, dbPass, dbHost, dbPort, dbName)
	db, err := gorm.Open(dbType, "host="+dbHost+" port="+dbPort+
		" user="+dbUser+" dbname="+dbName+" password="+dbPass)
	if err != nil {
		log.Fatal("Unable to Connect Database.\n", err)
		defer db.Close()
	}

	db.AutoMigrate(&models.Role{})
	db.AutoMigrate(&models.Account{})
	db.AutoMigrate(&models.Monthly{})

	roles := AddRole(db)
	AddAccount(db, *roles[0])
	return db
}

func StartServer(db *gorm.DB) {
	Host := viper.GetString(`server.host`)
	Port := viper.GetString(`server.port`)

	role := role.NewRoleRepository(db)
	accountRepo := account.NewAccountRepository(db)
	accountService := account.NewAccountService(accountRepo, role)

	monthlyRepo := monthly.NewMonthlyRepository(db)
	monthlyService := monthly.NewMonthlyService(monthlyRepo, accountService)

	app := iris.New()

	app.Logger().SetLevel("debug")
	//app.Use(recover.New())
	//app.Use(logger.New())

	/*jwtHandler := jwtmiddleware.New(jwtmiddleware.Config{
		ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
			return []byte("My Secret"), nil
		},
		// When set, the middleware verifies that tokens are signed with the specific signing algorithm
		// If the signing method is not constant the ValidationKeyGetter callback can be used to implement additional checks
		// Important to avoid security issues described here: https://auth0.com/blog/2015/03/31/critical-vulnerabilities-in-json-web-token-libraries/
		SigningMethod: jwt.SigningMethodHS256,
	})*/
	app.Use(func(context context.Context) {
		var jwtForm dto.BaseDTO
		context.ReadJSON(&jwtForm)

		token, _ := jwt.Parse(jwtForm.Token, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("There was an error")
			}
			return []byte("secret"), nil
		})
		logrus.Println(jwtForm.Token)
		logrus.Println(token.Valid)
		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			logrus.Println(claims)
			context.Next()
		} else {
			context.WriteString("Token invalid")
		}
	})

	baseRoute := app.Party(common.KEY_API_VERSION_V1)

	auth := baseRoute.Party("/auth")
	{
		auth.Post("/signup", accountService.SignUp)
		auth.Post("/signin", accountService.SignIn)
		auth.Post("/signout", accountService.SignOut)
		auth.Post("/testall", accountService.Fetch)
	}

	monthly := baseRoute.Party("/monthlys")
	{
		monthly.Post("/", monthlyService.CreateMonthly)
		monthly.Post("/{monthlyid:string}", monthlyService.DeleteMonthly)
		monthly.Get("/offset={page:int}", monthlyService.FindAllMonthly)
		monthly.Get("/{monthlyid:string}", monthlyService.FindByUid)
	}
	app.Get("/", cache.Handler(30*time.Second), writeMarkdown)
	app.Use()
	_ = app.Run(iris.Addr(Host+":"+Port),
		iris.WithConfiguration(iris.YAML("./iris.yml")),
		iris.WithoutServerError(iris.ErrServerClosed))
}
func writeMarkdown(ctx iris.Context) {
	// tap multiple times the browser's refresh button and you will
	// see this println only once every 10 seconds.
	var title = "<center><h1>"
	title += `صندوق قرض الحسنه حضرت ولی عصر عج`
	title += "</h1></center>"

	var markdownContents = []byte(title)

	_, _ = ctx.Markdown(markdownContents)
}
func AddAccount(db *gorm.DB, role models.Role) {

	var count = -1
	db.Table(models.Account.TableName(models.Account{})).Count(&count)

	//log.Infoln("Record Account Count: ", count)

	if count <= 0 {
		if db.HasTable(models.Account{}) == false {
			db.Table(models.Account.TableName(models.Account{})).CreateTable(&models.Account{})
		}
		var uid = uuid.NewV4().String()
		var username = "mahdihp"
		var password = "110110"
		var nationalcode = "0386007551"

		token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
			"username": username,
			"password": password,
		})
		tokenString, error := token.SignedString([]byte("secret"))
		if error != nil {
			fmt.Println(error)
		}

		var account = &models.Account{Uid: &uid, Username: &username, Password: &password, NationalCode: &nationalcode, Token: tokenString, Active: true,
			Roles: []models.Role{role}}

		log.Println("Insert Record Account Rows Affected :", db.Create(&account).RowsAffected)
	}
}

func AddRole(db *gorm.DB) []*models.Role {
	var role1 = models.Role{}
	var role2 = models.Role{}
	var role3 = models.Role{}
	roles := make([]*models.Role, 3)
	roles[0] = &role1
	roles[1] = &role2
	roles[2] = &role3
	var count = -1
	db.Table(models.Role.TableName(models.Role{})).Count(&count)
	//log.Infoln("Record Role Count: ", count)

	if count <= 0 {
		if db.HasTable(&models.Role{}) == false {
			db.Table(models.Role.TableName(models.Role{})).CreateTable(&models.Role{})
		}
		role1 = models.Role{Name: "Admin"}
		role2 = models.Role{Name: "User"}
		role3 = models.Role{Name: "Customer"}

		log.Infoln("Insert Record Role 1 Rows Affected :", db.Create(&role1).RowsAffected)
		log.Infoln("Insert Record Role 2 Rows Affected :", db.Create(&role2).RowsAffected)
		log.Infoln("Insert Record Role 3 Rows Affected :", db.Create(&role3).RowsAffected)
	}
	return roles
}
