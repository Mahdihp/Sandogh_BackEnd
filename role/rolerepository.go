package role

import (
	"Sandogh_BackEnd/models"
	"github.com/jinzhu/gorm"
)

type RoleRepository interface {
	Fetch() ([]*models.Role, bool)
	First(Name string) (models.Role, bool)
}

func NewRoleRepository(instance *gorm.DB) RoleRepository {
	return &roleRepository{Instance: instance}
}

type roleRepository struct {
	Instance *gorm.DB
}

func (this *roleRepository) First(Name string) (models.Role, bool) {
	var role models.Role
	this.Instance.Where("Name = ?", Name).First(&role)
	return role, true
}

func (this *roleRepository) Fetch() ([]*models.Role, bool) {
	roles := make([]*models.Role, 0)
	this.Instance.Find(&roles)
	//if len(roles) == 0 && err != nil {
	//	logrus.Error(err)
	//	return nil, nil
	//}
	return roles, true
}
