package monthly

import (
	"Sandogh_BackEnd/account"
	"Sandogh_BackEnd/common"
	"Sandogh_BackEnd/models"
	"Sandogh_BackEnd/models/dto"
	"github.com/asaskevich/govalidator"
	"github.com/kataras/iris"
	"github.com/satori/go.uuid"
)

type MonthlyService interface {
	CreateMonthly(ctx iris.Context)
	DeleteMonthly(ctx iris.Context)
	FindByUid(ctx iris.Context)
	FindAllMonthly(ctx iris.Context)
}
type monthlyService struct {
	Repo           MonthlyRepository
	AccountService account.AccountService
}

func NewMonthlyService(accountRepo MonthlyRepository, AccountService account.AccountService) MonthlyService {
	return &monthlyService{Repo: accountRepo, AccountService: AccountService}
}

func (this *monthlyService) CreateMonthly(ctx iris.Context) {
	var signupForm dto.MonthlyForm
	ctx.ReadForm(&signupForm)

	token := this.AccountService.VlidateToken(signupForm.AccountId, signupForm.Token)
	if !token {
		_, _ = ctx.JSON(dto.BaseDTO{
			Status:   200,
			Message:  common.KEY_INVALID_AUTHORIZATION_TOKEN,
			ObjectId: "",
			Active:   true,
		})
		return
	}

	bool, _ := govalidator.ValidateStruct(&signupForm)
	if bool {
		monthly := this.AccountService.FindByUid(signupForm.AccountId)
		if monthly != nil {
			var fullMonthly models.Monthly
			var uid = uuid.NewV4().String()
			fullMonthly.Uid = &uid
			fullMonthly.AccountID = &monthly.ID
			fullMonthly.AamountPerMonth = signupForm.AamountPerMonth
			this.Repo.Create(fullMonthly)
			_, _ = ctx.JSON(dto.BaseDTO{
				Status:   200,
				Message:  common.KEY_SIGNUP_SUCCESSFULLY,
				ObjectId: *fullMonthly.Uid,
				Active:   true,
			})
		} else {
			_, _ = ctx.JSON(dto.BaseDTO{
				Status:   200,
				Message:  common.KEY_NOT_FOUND_ACCOUNT,
				ObjectId: "",
				Active:   false,
			})
		}

	} else {
		_, _ = ctx.JSON(dto.BaseDTO{
			Status:   200,
			Message:  common.KEY_NOT_VALID_JSON,
			ObjectId: "",
			Active:   false,
		})
	}
}

func (this *monthlyService) DeleteMonthly(ctx iris.Context) {
	monthlyid := ctx.Params().Get("monthlyid")

	//println(strings.Split(varss,"=")[1])
	//monthlyid := strings.Split(varss, "=")[1]
	//fmt.Println("varss:", monthlyid) 09135338936

	//fmt.Println("query1", request.URL.Query().Get("monthlyid"))
	//fmt.Println("query2", request.URL.RawQuery)

	if monthlyid != "" {
		monthly, err := this.Repo.FindByUid(monthlyid)
		if err == nil {
			_ = this.Repo.RemoveMonthly(*monthly.Uid)
			_, _ = ctx.JSON(dto.BaseDTO{
				Status:   200,
				Message:  common.KEY_REMOVE_MONTHLY_SUCCESE,
				ObjectId: "",
				Active:   false,
			})
		} else {
			_, _ = ctx.JSON(dto.BaseDTO{
				Status:   200,
				Message:  common.KEY_NOT_FOUND_MONTHLY,
				ObjectId: "",
				Active:   false,
			})
		}

	} else {
		_, _ = ctx.JSON(dto.BaseDTO{
			Status:   200,
			Message:  common.KEY_NOT_FOUND_ACCOUNT,
			ObjectId: "",
			Active:   false,
		})
	}
}
func (this *monthlyService) FindByUid(ctx iris.Context) {
	monthlyid := ctx.Params().Get("monthlyid")
	if monthlyid != "" {
		monthly, err := this.Repo.FindByUid(monthlyid)
		accountid := int(*monthly.AccountID)
		if err == nil {
			_, _ = ctx.JSON(dto.MonthlyDTO{
				Status:          200,
				Message:         common.KEY_REMOVE_MONTHLY_SUCCESE,
				Active:          false,
				MonthlyId:       *monthly.Uid,
				AccountID:       string(accountid),
				AamountPerMonth: string(monthly.AamountPerMonth),
			})
		} else {
			_, _ = ctx.JSON(dto.BaseDTO{
				Status:   200,
				Message:  common.KEY_NOT_FOUND_MONTHLY,
				ObjectId: "",
				Active:   false,
			})
		}
	} else {
		_, _ = ctx.JSON(dto.BaseDTO{
			Status:   200,
			Message:  common.KEY_NOT_FOUND_MONTHLY,
			ObjectId: "",
			Active:   false,
		})
	}
}
func (this *monthlyService) FindAllMonthly(ctx iris.Context) {

}
