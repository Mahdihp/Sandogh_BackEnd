package monthly

import (
	"Sandogh_BackEnd/models"
	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
)

type MonthlyRepository interface {
	Create(models.Monthly)
	Fetch() ([]*models.Monthly, error)
	FetchAllByAccount(accountId uint) ([]*models.Monthly, error)
	FindByUid(uid string) (*models.Monthly, error)
	RemoveMonthly(uid string) error
}
type monthlyRepository struct {
	Instance *gorm.DB
}

func NewMonthlyRepository(db *gorm.DB) MonthlyRepository {
	return &monthlyRepository{Instance: db}
}
func (this *monthlyRepository) Create(models.Monthly) {

}

func (this *monthlyRepository) Fetch() ([]*models.Monthly, error) {
	monthly := make([]*models.Monthly, 0)
	//err := AccRepo.Instance.Find(&account).Error
	err := this.Instance.Model("monthlys").Find(&monthly).Error
	if len(monthly) == 0 {
		logrus.Error(err)
		return nil, nil
	}
	return monthly, nil
}

func (this *monthlyRepository) FetchAllByAccount(accountId uint) ([]*models.Monthly, error) {
	monthly := make([]*models.Monthly, 0)
	err := this.Instance.Where("id = ?", accountId).Find(&monthly).Error
	if len(monthly) == 0 {
		logrus.Error(err)
		return nil, nil
	}
	return monthly, nil
}

func (this *monthlyRepository) FindByUid(uid string) (*models.Monthly, error) {
	var monthly = models.Monthly{}
	err := this.Instance.Where("uid = ?", uid).First(&monthly).Error
	if err != nil {
		logrus.Error(err)
		return nil, nil
	}
	return &monthly, nil
}
func (this *monthlyRepository) RemoveMonthly(uid string) error {
	err := this.Instance.Delete("uid = ?", uid).Error
	if err != nil {
		logrus.Error(err)
		return err
	}
	return nil
}
