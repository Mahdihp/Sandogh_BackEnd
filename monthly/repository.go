package monthly

import (
	"Sandogh_BackEnd/models"
)

type Repository interface {
	Fetch(cursor string, num int64) (res []*models.Monthly, nextCursor string, err error)
	FindByUid(uid string) (*models.Monthly, error)
	//GetByTitle(ctx context.Context, title string) (*models.Monthly, error)
	//Update(ctx context.Context, ar *models.Monthly) error
	//Store(ctx context.Context, a *models.Monthly) error
	//Delete(ctx context.Context, id int64) error
}
