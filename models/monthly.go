package models

import "github.com/jinzhu/gorm"

type Monthly struct {
	gorm.Model
	Uid             *string `gorm:"unique;not null"`
	AccountID       *uint
	AamountPerMonth int32
}

func (Monthly) TableName() string {
	return "monthlys"
}
