package models

import (
	"github.com/jinzhu/gorm"
)

type Account struct {
	gorm.Model
	Uid           *string `gorm:"unique;not null"`
	Username      *string `gorm:"unique;not null;type:varchar(25)"`
	Password      *string `gorm:"unique;not null;type:varchar(25)"`
	AccountNumber *int64
	FirstName     string  `gorm:"type:varchar(255)"`
	LastName      string  `gorm:"type:varchar(255)"`
	DisplayName   string  `gorm:"type:varchar(255)"`
	FatherName    string  `gorm:"type:varchar(255)"`
	MobileNumber  string  `gorm:"type:varchar(255)"`
	NationalCode  *string `gorm:"unique;not null;type:varchar(10)"`
	City          string
	Adderss       string
	Token         string
	Active        bool
	CountLoan     int       // تعداد وام ها
	Roles         []Role    `gorm:"many2many:account_role;",json:"Roles,omitempty"`
	Monthlys      []Monthly `json:"Monthlys,omitempty"`
}

func (Account) TableName() string {
	return "accounts"
}
