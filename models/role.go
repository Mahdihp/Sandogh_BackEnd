package models

import "github.com/jinzhu/gorm"

type Role struct {
	gorm.Model
	Name    string    `gorm:"unique;not null;type:varchar(20)"`
	Account []Account `gorm:"many2many:account_role;"`
}

func (Role) TableName() string {
	return "roles"
}
