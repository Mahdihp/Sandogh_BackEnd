package dto

type MonthlyDTO struct {
	Status          int    `json:"status"`
	Message         string `json:"message"`
	Active          bool   `json:"active"`
	MonthlyId       string `json:"accountid,omitempty"`
	AccountID       string `json:"token,omitempty"`
	AamountPerMonth string `json:"token,omitempty"`
}
