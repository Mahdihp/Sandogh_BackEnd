package dto

type MonthlyForm struct {
	AccountId       string `valid:"required,omitempty"`
	AamountPerMonth int32  `valid:"required,omitempty"`
	ObjectId        string `valid:"omitempty"`
	Token           string `valid:"required,omitempty"`
}
