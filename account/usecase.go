package account

import (
	"Sandogh_BackEnd/models"
	"context"
)

type Usecase interface {
	Fetch(ctx context.Context, cursor string, num int64) ([]*models.Account, string, error)
	//GetByID(ctx context.Context, id int64) (*models.Account, error)
	//Update(ctx context.Context, ar *models.Account) error
	//GetByTitle(ctx context.Context, title string) (*models.Account, error)
	//Store(context.Context, *models.Account) error
	//Delete(ctx context.Context, id int64) error
}
