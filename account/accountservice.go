package account

import (
	"Sandogh_BackEnd/common"
	"Sandogh_BackEnd/models"
	"Sandogh_BackEnd/models/dto"
	"Sandogh_BackEnd/role"
	"fmt"
	"github.com/asaskevich/govalidator"
	"github.com/dgrijalva/jwt-go"
	"github.com/kataras/iris"
	"github.com/mitchellh/mapstructure"
	"github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/yaa110/go-persian-calendar"
)

type AccountService interface {
	SignUp(ctx iris.Context)
	SignIn(ctx iris.Context)
	SignOut(ctx iris.Context)
	VlidateToken(uid string, token string) bool
	ProtectedEndpoint(ctx iris.Context)
	FindByUid(uid string) *models.Account
	Fetch(ctx iris.Context)
}
type accountService struct {
	Repo     AccountRepository
	RoleRepo role.RoleRepository
}

func NewAccountService(Repo AccountRepository, RoleRepo role.RoleRepository) AccountService {
	return &accountService{Repo: Repo, RoleRepo: RoleRepo}
}

func (this *accountService) SignUp(ctx iris.Context) {
	var signupForm dto.SignUpForm
	ctx.ReadForm(&signupForm)
	//logrus.Println(signupForm)
	//ctx.ReadJSON(&signupForm)
	//logrus.Println(signupForm)

	bool, _ := govalidator.ValidateStruct(&signupForm)

	if bool {
		var count = this.Repo.CountByUserAndPassAndNati(signupForm.Username, signupForm.Password, signupForm.NationalCode)
		if count >= 1 {
			_, _ = ctx.JSON(dto.BaseDTO{
				Status:   200,
				Message:  common.KEY_USERNAME_PASSWORD_DUPLICATE,
				ObjectId: "",
				Active:   false,
			})
			return
		}

		var fullAccount models.Account
		var uid = uuid.NewV4().String()
		fullAccount.Uid = &uid
		fullAccount.Username = &signupForm.Username
		fullAccount.Password = &signupForm.Password
		fullAccount.DisplayName = signupForm.DisplayName
		fullAccount.MobileNumber = signupForm.MobileNumber
		fullAccount.NationalCode = &signupForm.NationalCode
		fullAccount.Active = signupForm.Active

		token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
			"username": signupForm.Username,
			"password": signupForm.Password,
		})
		tokenString, error := token.SignedString([]byte("secret"))
		if error != nil {
			fmt.Println(error)
		}
		fullAccount.Token = tokenString
		maxAccountNumber, _ := this.Repo.MaxAccountNumber()
		if maxAccountNumber == -1 {
			pt := ptime.Now(ptime.Iran())
			accountnumber := viper.GetInt64(`accountnumber.base`)
			*fullAccount.AccountNumber = accountnumber + int64(pt.Year()+pt.Day())
		} else {
			maxAccountNumber += 1
			fullAccount.AccountNumber = &maxAccountNumber
		}

		fullAccount.FirstName = signupForm.FirstName
		fullAccount.LastName = signupForm.LastName
		fullAccount.FatherName = signupForm.FatherName
		fullAccount.City = signupForm.City
		fullAccount.Adderss = signupForm.Adderss

		var role models.Role
		if signupForm.TypeUser == 1 { // Admin

			role, _ = this.RoleRepo.First("Admin")
			fullAccount.Roles = []models.Role{role}
			this.Repo.Create(fullAccount)

		} else if signupForm.TypeUser == 2 { //User

			role, _ = this.RoleRepo.First("User")
			fullAccount.Roles = []models.Role{role}
			this.Repo.Create(fullAccount)

		} else if signupForm.TypeUser == 3 { // customer

			role, _ = this.RoleRepo.First("Customer")
			fullAccount.Roles = []models.Role{role}
			this.Repo.Create(fullAccount)
		}
		_, _ = ctx.JSON(dto.BaseDTO{
			Status:   200,
			Message:  common.KEY_SIGNUP_SUCCESSFULLY,
			ObjectId: *fullAccount.Uid,
			Active:   true,
		})
	} else {
		_, _ = ctx.JSON(dto.BaseDTO{
			Status:   200,
			Message:  common.KEY_NOT_VALID_JSON,
			ObjectId: "",
			Active:   false,
		})
	}

}
func (this *accountService) FindByUid(uid string) *models.Account {
	account := this.FindByUid(uid)
	if account != nil {
		return account
	}
	return nil
}
func (this *accountService) SignIn(ctx iris.Context) {
	var account dto.LoginForm
	ctx.ReadForm(&account)
	fullAccount, e := this.Repo.FindByUsernameAndPassword(account.Username, account.Password)
	if e == nil {
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
			"username": account.Username,
			"password": account.Password,
		})
		tokenString, error := token.SignedString([]byte("secret"))
		if error != nil {
			fmt.Println(error)
		}

		fullAccount.Token = tokenString
		logrus.Infoln(fullAccount)

		this.Repo.UpdateTokenAccount(*fullAccount.Uid, tokenString)
		_, _ = ctx.JSON(dto.BaseDTO{
			Status:   200,
			Message:  common.KEY_SIGNIN_SUCCESSFULLY,
			ObjectId: *fullAccount.Uid,
			Active:   true,
			Token:    tokenString,
		})
	} else {
		_, _ = ctx.JSON(dto.BaseDTO{
			Status:   200,
			Message:  common.KEY_USERNAME_PASSWORD_INCORRECT,
			ObjectId: "",
			Active:   false,
			Token:    "",
		})
	}

}
func (this *accountService) SignOut(ctx iris.Context) {

	var jwtForm dto.BaseDTO
	ctx.ReadJSON(&jwtForm)

	token, _ := jwt.Parse(jwtForm.Token, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("There was an error")
		}
		return []byte("secret"), nil
	})
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		var account models.Account
		mapstructure.Decode(claims, &account)
		if *account.Uid != "" {
			acc, e := this.Repo.FindByUidAndUsernameAndPassword(*account.Uid, *account.Username, *account.Password)
			if e != nil {
				_, _ = ctx.JSON(dto.BaseDTO{
					Status:   200,
					Message:  common.KEY_USERNAME_PASSWORD_INCORRECT,
					ObjectId: "",
					Active:   false,
					Token:    "",
				})
			} else {
				this.Repo.SingOutAccount(*acc.Uid)
				_, _ = ctx.JSON(dto.BaseDTO{
					Status:   200,
					Message:  common.KEY_SIGNOUT,
					ObjectId: "",
					Active:   false,
					Token:    "",
				})
			}
		}
	} else {
		_, _ = ctx.JSON(dto.BaseDTO{
			Status:   200,
			Message:  common.KEY_INVALID_AUTHORIZATION_TOKEN,
			ObjectId: "",
			Active:   false,
		})
	}

}

func (this *accountService) VlidateToken(uid string, token string) bool {

	fullaccount, _ := this.Repo.FindByUidAndToken(uid, token)
	if *fullaccount.Uid != "" {
		token, _ := jwt.Parse(fullaccount.Token, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("There was an error")
			}
			return []byte("secret"), nil
		})
		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			var account models.Account
			mapstructure.Decode(claims, &account)
			if account.Uid == fullaccount.Uid && account.Username == fullaccount.Username && account.Password == fullaccount.Password {
				return true

			} else {
				return false
			}
		} else {
			return false
		}

	} else {
		return false
	}
}
func (this *accountService) ProtectedEndpoint(ctx iris.Context) {
	params := ctx.Params().Get("token") // request.URL.Query()
	//ctx.ReadJSON(&jwtForm)

	token, _ := jwt.Parse(params, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("There was an error")
		}
		return []byte("secret"), nil
	})
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		var user models.Account
		mapstructure.Decode(claims, &user)
		_, _ = ctx.JSON(user)
	} else {
		_, _ = ctx.JSON(dto.BaseDTO{
			Status:   200,
			Message:  "Invalid authorization token",
			ObjectId: "",
			Active:   false,
		})
	}
}

func (this *accountService) Fetch(ctx iris.Context) {
	accounts, _ := this.Repo.Fetch()
	_, _ = ctx.JSON(accounts)
}
