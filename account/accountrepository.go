package account

import (
	"Sandogh_BackEnd/models"
	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
	"sync"
)

type AccountRepository interface {
	Create(account models.Account)
	Fetch() (accounts []models.Account, err error)
	FindByUsernameAndPassword(username string, password string) (models.Account, error)
	FindByUidAndToken(uid string, token string) (models.Account, error)
	CountByUserAndPassAndNati(username string, password string, nationalcode string) int
	FindByUidAndUsernameAndPassword(uid string, username string, password string) (models.Account, error)
	UpdateTokenAccount(uid string, token string)
	SingOutAccount(uid string)
	MaxAccountNumber() (int64, error)
}

func NewAccountRepository(instance *gorm.DB) AccountRepository {
	return &accountRepository{Instance: instance}
}

type accountRepository struct {
	Instance *gorm.DB
	mu       sync.RWMutex
}

func (this *accountRepository) Create(account models.Account) {

}

func (this *accountRepository) Fetch() ([]models.Account, error) {
	account := make([]models.Account, 0)
	//err := this.Instance.Find(&account).Error
	err := this.Instance.Preload("Role").Find(&account).Error
	if len(account) == 0 {
		logrus.Error(err)
		return nil, nil
	}
	return account, nil
}
func (this *accountRepository) FindByUsernameAndPassword(username string, password string) (models.Account, error) {
	var account = models.Account{}
	this.Instance.Where("username = ? AND password = ? ", username, password).First(&account)
	//if err != nil {
	//	logrus.Error(err)
	//	return nil, nil
	//}
	return account, nil
}

func (this *accountRepository) FindByUidAndToken(uid string, token string) (models.Account, error) {
	var account = models.Account{}
	this.Instance.Where("uid = ? AND token = ? ", uid, token).First(&account)
	//if err != nil {
	//	logrus.Error(err)
	//	return nil, nil
	//}
	return account, nil
}
func (this *accountRepository) CountByUserAndPassAndNati(username string, password string, nationalcode string) int {
	var count int = -1
	this.Instance.Model(&models.Account{}).Where("Username = ?", username).Or("Password = ?", password).Or("national_code = ?", nationalcode).Count(&count)
	return count
}

func (this *accountRepository) FindByUidAndUsernameAndPassword(uid string, username string, password string) (models.Account, error) {
	var account = models.Account{}
	this.Instance.Where("uid = ? AND username = ? AND password = ? ", uid, username, password).First(&account)
	//if err != nil {
	//	logrus.Error(err)
	//	return nil, nil
	//}
	return account, nil
}

func (this *accountRepository) FindByUid(uid string) (models.Account, error) {
	var account = models.Account{}
	this.Instance.Where("uid = ?", uid).First(&account)
	//if err != nil {
	//	logrus.Error(err)
	//	return nil, nil
	//}
	return account, nil
}
func (this *accountRepository) UpdateTokenAccount(uid string, token string) {
	var account = models.Account{}
	err := this.Instance.Where("uid = ?", uid).First(&account).Error
	if err == nil {
		account.Token = token
		this.Instance.Update(&account)
	}
}
func (this *accountRepository) SingOutAccount(uid string) {
	var account = models.Account{}
	err := this.Instance.Where("uid = ?", uid).First(&account).Error
	if err == nil {
		account.Token = ""
		this.Instance.Update(&account)
	}
}
func (this *accountRepository) MaxAccountNumber() (int64, error) {

	//db.Model(&User{}).Where("name = ?", "jinzhu").Count(&count)
	type Max struct{ M int64 }
	var max Max
	err := this.Instance.Raw("SELECT MAX(account_number) as M FROM Accounts").Scan(&max).Error
	if err != nil {
		logrus.Error(err)
		return -1, nil
	}
	return max.M, nil
}
